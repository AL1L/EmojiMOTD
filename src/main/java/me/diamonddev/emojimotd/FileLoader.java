package me.diamonddev.emojimotd;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.server.ServerListPingEvent;

public class FileLoader {

	private static Logger logger = Bukkit.getServer().getLogger();
	private static File dataFolder = Plugin.gi().getDataFolder();

	private static String[] internalEmojis = { "grinning face.png", "grinning face with smiling eyes.png",
			"face with tears of joy.png", "smiling face with open mouth.png",
			"smiling face with open mouth and smiling eyes.png", "smiling face with open mouth and cold sweat.png",
			"smiling face with open mouth and tightly-closed eyes.png", "winking face.png",
			"smiling face with smiling eyes.png", "face savouring delicious food.png",
			"smiling face with sunglasses.png", "smiling face with heart-shaped eyes.png", "face throwing a kiss.png",
			"kissing face.png", "kissing face with smiling eyes.png", "kissing face with closed eyes.png",
			"white smiling face.png", "smiling face with halo.png", "neutral face.png", "expressionless face.png",
			"face without mouth.png", "face with rolling eyes.png", "smirking face.png", "persevering face.png",
			"disappointed but relieved face.png", "face with open mouth.png", "hushed face.png", "sleepy face.png",
			"tired face.png", "sleeping face.png", "relieved face.png", "face with stuck-out tongue.png",
			"face with stuck-out tongue and winking eye.png", "face with stuck-out tongue and tightly-closed eyes.png",
			"unamused face.png", "face with cold sweat.png", "pensive face.png", "confused face.png",
			"confounded face.png", "face with medical mask.png", "astonished face.png", "disappointed face.png",
			"worried face.png", "face with look of triumph.png", "crying face.png", "loudly crying face.png",
			"frowning face with open mouth.png", "anguished face.png", "fearful face.png", "weary face.png",
			"grimacing face.png", "face with open mouth and cold sweat.png", "face screaming in fear.png",
			"flushed face.png", "dizzy face.png", "pouting face.png", "angry face.png" };

	public static void CheckFiles() {
		boolean debug = Plugin.gi().getConfig().getBoolean("Options.Debug");
		if (debug)
			logger.info("[EmojiMOTD] Checking Files...");
		String dnlFilePath = "Delete_This_To_Recopy_Icons";
		File downloadFile = new File(dataFolder, dnlFilePath);
		if (!downloadFile.exists()) {
			if (debug)
				logger.info("[EmojiMOTD] Copying Files...");
			for (int i = 0; i < internalEmojis.length; i++) {
				String filePath = "icons/" + internalEmojis[i];
				saveResource(filePath, false);
			}
			try {
				downloadFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (debug)
			logger.info("[EmojiMOTD] Done!");
	}

	public static void logPingEvent(ServerListPingEvent e, String iconName) {
		boolean logPing = Plugin.gi().getConfig().getBoolean("Options.LogPings", false);
		List<String> lines;

		DateFormat df = new SimpleDateFormat("[dd/MM/yy HH:mm:ss] ");
		Date dateobj = new Date();
		String dateTime = df.format(dateobj);

		String l1 = "Server List Ping From \"" + e.getAddress().getHostAddress() + "\"";
		String[] motds = e.getMotd().split("\n");
		String motd = "";
		for (int i = 0; i < motds.length; i++) {
			motd = motd + motds[i].trim() + " ";
		}
		String l2 = "  > Setting motd to \"" + motd.trim() + ChatColor.RESET + "\"";
		String l3 = "  > Setting online players to \"" + e.getNumPlayers() + "\"";
		String l4 = "  > Setting max players to \"" + e.getMaxPlayers() + "\"";
		String l5 = "  > Setting icon to \"" + iconName + "\"";
		lines = Arrays.asList(l1, l2, l3, l4, l5);
		for (String str : lines) {
			if (logPing) {
				Bukkit.getConsoleSender().sendMessage(str);
			}
		}
		if (logPing) {
		}
		try {
			String logFilePath = "pingLogs.log";
			File logFile = new File(dataFolder, logFilePath);
			if (!logFile.exists())
				logFile.createNewFile();
			Path logPath = Paths.get(logFile.getAbsolutePath());

			lines = Arrays.asList(dateTime + l1, dateTime + l2, dateTime + l3, dateTime + l4, dateTime + l5);
			Files.write(logPath, lines, Charset.forName("UTF-8"), StandardOpenOption.APPEND);

		} catch (Exception e1) {
			boolean debug = Plugin.gi().getConfig().getBoolean("Options.Debug");
			Bukkit.getLogger().log(Level.WARNING, "[EmojiMOTD] Could not log ping");
			if (debug)
				e1.printStackTrace();
		}
	}

	private static void saveResource(String resourcePath, boolean replace) {
		if (resourcePath == null || resourcePath.equals("")) {
			throw new IllegalArgumentException("ResourcePath cannot be null or empty");
		}

		resourcePath = resourcePath.replace('\\', '/');
		InputStream in = Plugin.gi().getResource(resourcePath);
		if (in == null) {
			throw new IllegalArgumentException("The embedded resource '" + resourcePath + "' cannot be found");
		}

		File outFile = new File(dataFolder, resourcePath);
		int lastIndex = resourcePath.lastIndexOf('/');
		File outDir = new File(dataFolder, resourcePath.substring(0, lastIndex >= 0 ? lastIndex : 0));

		if (!outDir.exists()) {
			outDir.mkdirs();
		}

		try {
			if (!outFile.exists() || replace) {
				OutputStream out = new FileOutputStream(outFile);
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				out.close();
				in.close();
			} else {
				/*
				 * logger.log(Level.WARNING, "Could not save " +
				 * outFile.getName() + " to " + outFile + " because " +
				 * outFile.getName() + " already exists.");
				 */
			}
		} catch (IOException ex) {
			logger.log(Level.SEVERE, "Could not save " + outFile.getName() + " to " + outFile, ex);
		}
	}
}
